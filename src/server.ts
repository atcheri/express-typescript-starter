import dotenv from 'dotenv';
dotenv.config();

import errorHandler from 'errorhandler';
import app from './app';

const nodeEnv = process.env.NODE_ENV || 'development';
const port = process.env.PORT || 3000;

if (nodeEnv === 'development') {
  app.use(errorHandler());
}

const server = app.listen(port, () => {
  console.log('Express server app running at http://localhost:%d in %s mode', port, app.get('env'));
  console.log('Press CTRL-C to stop\n');
});

export default server;
